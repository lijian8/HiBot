//获取数据并显示数据表格
function GetTableData(tableId, ChlickEvent) {
	var table = $(tableId);
	var url = table.data('url');
	$
			.ajax({
				url : url,
				type : 'get',
				dataType : 'json',
			})
			.done(
					function(json) {
						var fileds = new Array();
						table.children('thead').children('tr').children('th')
								.each(function(index, el) {
									var field = $(this).data('field');
									fileds[index] = field;
								});
						var tbody = '';
						$
								.each(
										json,
										function(index, el) {
											var tr = "<tr>";
											$
													.each(
															fileds,
															function(i, el) {
																if (fileds[i] == "state") {
																	if (json[index][fileds[i]] == 0) {
																		tr += '<td class="am-hide-sm-only">online</td>';
																	} else {
																		tr += '<td class="am-hide-sm-only">offline</td>';
																	}
																} else if (fileds[i] == "operation") {
																	tr += '<td>'
																			+ '<div class="am-btn-toolbar">'
																			+ '<div class="am-btn-group am-btn-group-xs">'
																			+ '<button class="am-btn am-btn-default am-btn-xs am-text-secondary">'
																			+ '<span class="am-icon-pencil-square-o"></span>编辑'
																			+ '</button>'
																			/*
																			 * + '<button
																			 * class="am-btn
																			 * am-btn-default
																			 * am-btn-xs
																			 * am-text-secondary"
																			 * onclick="location.href=\'./task-monitor.html\'">'
																			 */
																			+ '<button class="am-btn am-btn-default am-btn-xs am-text-secondary">'
																			+ '<span class="am-icon-pencil-square-o"></span> <a href="task-monitor.html?id='
																			+ json[index].id
																			+ '">控制</a>'
																			+ '</button>'
																			+ '<button class="am-btn am-btn-default am-btn-xs am-text-secondary">'
																			+ '<span class="am-icon-pencil-square-o"></span> <a href="task-control.html?id='
																			+ json[index].id
																			+ '">监控</a>'
																			+ '</button>'
																			+ '<button class="am-btn am-btn-default am-btn-xs am-text-secondary">'
																			+ '<class="am-icon-pencil-square-o"></span> <a href="../ui/map/map.html?id='
																			+ json[index].id
																			+ '">应用管理</a>'
																			+ '</button>'
																			+ '</div>'
																			+ '</div>'
																			+ '</td>'
																} else if (fileds[i] == "checkbox") {
																	tr += '<td><input type="checkbox" name="choose" value='
																			+ json[index].id
																			+ '></td>'
																} else if (fileds[i]) {
																	tr += '<td>'
																			+ json[index][fileds[i]]
																			+ '</td>';
																}
															});
											tr += "</tr>";
											tbody += tr;
										});
						table.children('tbody').append(tbody);

						/*
						 * if (ChlickEvent) {// 如果需要支持行选中事件
						 * table.children('tbody').addClass('sel');
						 * table.children('tbody.sel').children('tr') .click(
						 * function(event) { $(this).siblings('tr').removeClass(
						 * 'active');// 删除其他行的选中效果 $(this).addClass('active');//
						 * 增加选中效果 ChlickEvent($(this)
						 * .children('td:eq(0)').text());// 新增点击事件 }); }
						 */
					}).fail(function() {
				alert("Err");
			});
}
// 格式化JSON数据，比如日期
// function formatJsonData(jsondata) {
// if (jsondata == null) {
// return '无数据';
// } else if (/\/Date\(\d+\)/.exec(jsondata)) {
// var date = new Date(parseInt(jsondata.replace("/Date(", "").replace(
// ")/", ""), 10));
// return date.toLocaleString();
// }
// return jsondata;
// }

// 全选全不选table中的checkbox
function DoCheck() {
	var ch = document.getElementsByName("choose");
	var num = $('input[name="choose"]', '#myModal').length;
	if (document.getElementsByName("allChecked")[0].checked == true) {
		for (var i = 0; i < ch.length - num; i++) {
			ch[i].checked = true;
		}
	} else {
		for (var i = 0; i < ch.length - num; i++) {
			ch[i].checked = false;
		}
	}

	if (document.getElementsByName("allChecked")[1].checked == true) {
		for (var i = ch.length - num; i < ch.length; i++) {
			ch[i].checked = true;
		}
	} else {
		for (var i = ch.length - num; i < ch.length; i++) {
			ch[i].checked = false;
		}
	}
}

// 获取选中的client_id和task_id值

function Distribute() {
	var check = $('input[name="choose"]');
	var client_ids = "";
	var task_ids = "";
	var i = 0;
	var j = 0;
	/* alert($('input[name="choose"]:checked', '#Client').length); */
	$('input[name="choose"]:checked', '#Client').each(function() {
		if (client_ids == "") {
			client_ids = $(this).val();
		} else {
			client_ids = client_ids + "," + $(this).val();
		}
	});
	$('input[name="choose"]:checked', '#Task').each(function() {
		if (task_ids == "") {
			task_ids = $(this).val();
		} else {
			task_ids = task_ids + "," + $(this).val();
		}
	});

	/*
	 * console.log(client_ids); console.log(task_ids);
	 */

	// 将client_ids和task_ids上传到后台
	var url = "http://localhost:8080/HiBotServer/api/v1/task/publish/taskIds/"
			+ task_ids + "/clientIds/" + client_ids;
	console.log(url);

	$.ajax({
		url : url,
		type : 'get',
		success : function() {
			alert("distributing");
		},
		error : function() {
			alert("error");
		}
	});


}
