package cn.ybz21.hibot.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import cn.ybz21.hibot.bean.App;

public class AppDAO extends BaseDAO{
	private String NAME_TABLE = "app_server";

	public App queryAppById(String id) {
		App tempApp = null;
		String sql = "select * from  " + NAME_TABLE + "  where id=?";
		Connection conn = db.getConnection();
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			pstmt = conn.prepareStatement(sql);
			int index = 1;
			pstmt.setObject(index++, id);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				tempApp = new App(rs);
			}
		} catch (SQLException e) {
			e.printStackTrace();

		}
		db.close(conn, pstmt, rs);
		return tempApp;
	}

	/**
	 * 获取分页任务
	 * 
	 * @param startPage
	 * @param pageSize
	 * @return
	 */
	public List<App> queryApps(int startPage, int pageSize) {
		int pageIndex = (startPage - 1) * pageSize;
		List<App> Apps = new ArrayList<App>();
		String sql = "select * from " + NAME_TABLE + " limit ?,?";
		Connection conn = db.getConnection();
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			pstmt = conn.prepareStatement(sql);
			int index = 1;
			pstmt.setInt(index++, pageIndex);
			pstmt.setInt(index++, pageSize);
			rs = pstmt.executeQuery();
			while (rs.next()) {

				App tempApp = new App(rs);
				Apps.add(tempApp);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		db.close(conn, pstmt, rs);
		return Apps;
	}
}
