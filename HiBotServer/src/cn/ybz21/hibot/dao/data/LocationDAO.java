package cn.ybz21.hibot.dao.data;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import cn.ybz21.hibot.bean.Robot;
import cn.ybz21.hibot.bean.data.Location;
import cn.ybz21.hibot.dao.BaseDAO;
import cn.ybz21.hibot.dao.ClientDAO;
/**
 * 地理位置数据存取
 * @author ybz
 * @email bingzheng_yan@163.com
 * @time 2017年6月28日
 */
public class LocationDAO extends BaseDAO {
	public String NAME_TABLE = "data_location";

	/**
	 * 
	 * @param client
	 * @return
	 */
	public boolean insertClient(Location loc) {
		boolean flag = false;
		String sql = "insert into " + NAME_TABLE
				+ " (client_id,latitude,longitude,time_upload) values(?,?,?,?)";
		Connection conn = db.getConnection();
		PreparedStatement pstmt = null;
		try {
			pstmt = conn.prepareStatement(sql);
			int index = 1;
			pstmt.setObject(index++, loc.clientId);
			pstmt.setObject(index++, loc.latitude);
			pstmt.setObject(index++, loc.longitude);
			
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String timeString = sdf.format(new Date());
			
			loc.timeUpload=timeString;
			
			pstmt.setObject(index++, loc.timeUpload);
			int i = pstmt.executeUpdate();
			if (i > 0)
				flag = true;
			else
				flag = false;
		} catch (SQLException e) {
			e.printStackTrace();
			flag = false;
		}
		db.close(conn, pstmt, null);
		
		ClientDAO cd=new ClientDAO();
		Robot c=cd.queryClientById(loc.clientId);
		System.out.println("--:"+loc.timeUpload);
		c.location=loc;
		cd.updateClient(c);
		return flag;
	}
	
	public List<Location> queryAllLast() {
		List<Robot> list1 = new ClientDAO().queryAllClient(1, 20);
		List<Location> list = new ArrayList<Location>();
		for(Robot c:list1){
//			System.out.println(c.location.latitude);
			list.add(c.location);}
		return list;
	}
	public List<Location> queryAllById(String id) {
		List<Location> list = new ArrayList<Location>();
		String sql = "select * from  " + NAME_TABLE
				+  " where client_id=? order by time_upload desc limit 0,30";
		Connection conn = db.getConnection();
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			pstmt = conn.prepareStatement(sql);
			int index = 1;
			pstmt.setObject(index++, id);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				Location temp = new Location(rs, NAME_TABLE);
				list.add(temp);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		db.close(conn, pstmt, rs);
		return list;
	}

	public Location queryLast(String id) {
		Location temp = null;
		String sql = "select  * from  " + NAME_TABLE + " where client_id=? order by time_upload desc limit 0,1";
		System.out.println(sql);
		Connection conn = db.getConnection();
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			pstmt = conn.prepareStatement(sql);
			int index = 1;
			pstmt.setObject(index++, id);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				temp = new Location(rs, NAME_TABLE);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		db.close(conn, pstmt, rs);
		return temp;
	}
}
