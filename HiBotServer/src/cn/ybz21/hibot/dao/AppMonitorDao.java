package cn.ybz21.hibot.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import cn.ybz21.hibot.bean.App;
import cn.ybz21.hibot.bean.AppMonitor;

import com.google.gson.Gson;

public class AppMonitorDao extends BaseDAO {
	private String NAME_TABLE = "app_monitor";

	public boolean insert(AppMonitor appMonitor) {
		boolean flag = false;
		String sql = "insert into " + NAME_TABLE
				+ " (client_id,apps,date) values(?,?,?)";
		Connection conn = db.getConnection();
		PreparedStatement pstmt = null;
		try {
			pstmt = conn.prepareStatement(sql);
			int index = 1;
			pstmt.setObject(index++, appMonitor.clientId);
			pstmt.setObject(index++, new Gson().toJson(appMonitor.apps));
			pstmt.setObject(index++, appMonitor.date);
			int i = pstmt.executeUpdate();
			if (i > 0)
				flag = true;
			else
				flag = false;
		} catch (SQLException e) {
			e.printStackTrace();
			flag = false;
		}
		db.close(conn, pstmt, null);
		return flag;
	}

	public boolean update(AppMonitor appMonitor) {
		boolean flag = false;
		String sql = "update  " + NAME_TABLE
				+ "  set apps=?, date=?  where client_id=?";
		Connection conn = db.getConnection();
		PreparedStatement pstmt = null;
		try {
			pstmt = conn.prepareStatement(sql);
			int index = 1;

			pstmt.setObject(index++, new Gson().toJson(appMonitor.apps));
			pstmt.setObject(index++, appMonitor.date);
			pstmt.setObject(index++, appMonitor.clientId);
			int i = pstmt.executeUpdate();
			if (i > 0)
				flag = true;
			else
				flag = false;
		} catch (SQLException e) {
			e.printStackTrace();
			flag = false;
		}
		db.close(conn, pstmt, null);
		return flag;
	}

	public boolean isExist(AppMonitor appMonitor) {
		boolean flag = false;
		String sql = "select count(*) as num from  " + NAME_TABLE
				+ "  where client_id = ? ";
		Connection conn = db.getConnection();
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		int num = 0;
		try {
			pstmt = conn.prepareStatement(sql);
			int index = 1;
			pstmt.setObject(index++, appMonitor.clientId);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				num = rs.getInt("num");
			}
		} catch (SQLException e) {
			e.printStackTrace();
			flag = false;
		}
		db.close(conn, pstmt, null);
		if (num > 0)
			flag = true;
		return flag;
	}

	public List<App> queryByClientId(String clientId) {
		AppMonitor temp = null;
		String sql = "select * from  " + NAME_TABLE + "  where client_id=?";
		Connection conn = db.getConnection();
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			pstmt = conn.prepareStatement(sql);
			int index = 1;
			pstmt.setObject(index++, clientId);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				temp = new AppMonitor(rs);
			}
		} catch (SQLException e) {
			e.printStackTrace();

		}
		db.close(conn, pstmt, rs);
		return temp.apps;
	}

	public App getAppDetail(String clientId, String name) {
		App ret = null;
		List<App> apps = queryByClientId(clientId);
		for (App app : apps) {
			if (app.name.equals(name)) {
				ret = app;
				break;
			}
		}
		return ret;
	}

}
