package cn.ybz21.hibot.service;

import java.util.List;

import cn.ybz21.hibot.bean.App;
import cn.ybz21.hibot.bean.AppMonitor;
import cn.ybz21.hibot.dao.AppMonitorDao;

public class AppMonitorService {
	AppMonitorDao appMonitorDao;

	public AppMonitorService() {
		appMonitorDao = new AppMonitorDao();
	}

	public void AddAppMonitor(AppMonitor appMonitor) {
		// 添加监控记
		if (appMonitorDao.isExist(appMonitor))
			appMonitorDao.update(appMonitor);
		else
			appMonitorDao.insert(appMonitor);
	}

	public List<App> getMonitorAppsByClientId(String clientId) {
		return appMonitorDao.queryByClientId(clientId);
	}

	public App getAppDetail(String clientId, String name) {
		return appMonitorDao.getAppDetail(clientId, name);
	}
}
