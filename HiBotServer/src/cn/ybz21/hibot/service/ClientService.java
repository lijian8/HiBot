package cn.ybz21.hibot.service;

import cn.ybz21.hibot.bean.Robot;
import cn.ybz21.hibot.dao.ClientDAO;

public class ClientService {

	public void saveLinkClient(Robot c) {
		ClientDAO dao = new ClientDAO();
		Robot temp = dao.queryClientByIdMac(c.idMac);
		if (temp == null) {
			dao.insertClient(c);
		} else {
			c.state=Robot.STATE_ONLINE;
			dao.updateClient(c);
		}

	}

	public void deletLinkClient(Robot c) {
		ClientDAO dao = new ClientDAO();
		Robot temp = dao.queryClientByIdMac(c.idMac);
		if (temp != null) {
			c.state=Robot.STATE_OFFLINE;
			dao.updateClient(c);
		}
	}
}
