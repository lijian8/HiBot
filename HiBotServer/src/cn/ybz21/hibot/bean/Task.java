package cn.ybz21.hibot.bean;

import java.io.Serializable;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Task implements Serializable {
	private static final long serialVersionUID = -8836233630632945589L;
	public String id;
	public String name;
	public String programUrl;
	
	public Task(){
	}
	public Task(ResultSet resultSet) throws SQLException{
		super();
		id = resultSet.getString("tasks.id");
		name = resultSet.getString("tasks.name");
		programUrl = resultSet.getString("tasks.path");
	}
}
