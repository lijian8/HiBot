package cn.ybz21.hibot.bean;

import java.io.Serializable;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class AppMonitor implements Serializable {
	private static final long serialVersionUID = 3288358431579895290L;

	public String clientId;
	public List<App> apps;
	public String date;

	public AppMonitor(ResultSet rs) {
		try {
			clientId = rs.getString("client_id");
			String tmp = rs.getString("apps");
			apps = new Gson().fromJson(tmp, new TypeToken<List<App>>() {
			}.getType());
			date = rs.getString("date");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public AppMonitor() {

	}

}
