package cn.ybz21.hibot.bean.data;

import java.io.Serializable;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Location implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -1944803949999954892L;
	public String clientId;
	public double latitude;
	public double longitude;
	public String timeUpload;
	
	public Location(){
		
	}
	
	public Location(ResultSet resultSet,String tableName) throws SQLException {
		super();
		clientId = resultSet.getString(tableName+".client_id");
		latitude = resultSet.getDouble(tableName+".latitude");
		longitude = resultSet.getDouble(tableName+".longitude");
		timeUpload = resultSet.getString(tableName+".time_upload");
	
	}

	@Override
	public String toString() {
		return "Location [clientId=" + clientId + ", latitude=" + latitude
				+ ", longitude=" + longitude + ", timeUpload=" + timeUpload
				+ "]";
	}
	
	
}
