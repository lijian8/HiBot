package cn.ybz21.hibot.resource;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import cn.ybz21.hibot.bean.ResultCode;
import cn.ybz21.hibot.bean.Task;
import cn.ybz21.hibot.util.StaticValues;
import cn.ybz21.hibot.util.Temp;

@Path("/polling")
public class PollingTemp {
	/**
	 * http://localhost:8080/HiBotServer/api/v1/polling/id/1
	 * 
	 * @param idMac
	 * @return
	 */
	@GET
	@Path("/id/{idMac}")
	@Produces(MediaType.APPLICATION_JSON)
	public Task publishTask(@PathParam("idMac") String idMac) {
		
		Task task = Temp.maps.get(idMac);
		if (task == null) {
			task = new Task();
			task.id = "-1";
		} else {
			System.out.println("not null");
			Temp.maps.remove(idMac);
		}
		return task;
	}

	/**
	 * http://localhost:8080/HiBotServer/api/v1/polling/setid/1
	 * 
	 * @param idMac
	 * @return
	 */
	@GET
	@Path("/setid/{idMac}")
	@Produces(MediaType.APPLICATION_JSON)
	public ResultCode setIds(@PathParam("idMac") String idMac) {
		ResultCode rCode = new ResultCode();
		String[] ids = idMac.split(",");
		for (String id : ids) {
			Task task = new Task();
			task.id = id;
			task.name = "myros";
			task.programUrl = StaticValues.IP_HTTP_SERVER + "/task/myros.zip";
			Temp.maps.put(id, task);

		}
		return rCode;
	}
}
