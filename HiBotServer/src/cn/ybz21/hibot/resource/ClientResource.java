package cn.ybz21.hibot.resource;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import cn.ybz21.hibot.bean.Robot;
import cn.ybz21.hibot.dao.ClientDAO;

@Path("/client")
public class ClientResource {
	
	@GET
	@Produces(MediaType.TEXT_PLAIN)
	public String getDefaultTask() {
		return "client  center";
	}
	
	/**	 
	 *<p> http://127.0.0.1:8080/HiBotServer/api/v1/client/id/1</p>
	 *
	 * @param id
	 * @return
	 */
	@GET
	@Path("/id/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Robot getClientById(@PathParam("id") String id) {
		return new ClientDAO().queryClientById(id);
	}

	/**
	 *<p> http://127.0.0.1:8080/HiBotServer/api/v1/client/list?startPage=1&pageSize=100</p>
	 *
	 * @param startPage
	 * @param pageSize
	 * @return
	 */
	@GET
	@Path("/list")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Robot> getClientList(
			@QueryParam(value = "startPage") int startPage,
			@QueryParam(value = "pageSize") int pageSize) {
		if(pageSize==0){
			pageSize=100;
			startPage=1;}
		return new ClientDAO().queryAllClient(startPage, pageSize);
	}
}