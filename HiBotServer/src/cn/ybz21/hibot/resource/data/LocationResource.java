package cn.ybz21.hibot.resource.data;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import cn.ybz21.hibot.bean.ResultCode;
import cn.ybz21.hibot.bean.data.Location;
import cn.ybz21.hibot.dao.data.LocationDAO;

@Path("/location")
public class LocationResource {
	/**
	 * http://127.0.0.1:8080/HiBotServer/api/v1/location
	 * @return
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Location getDefaultLocation() {
		return new Location();
	}

	/**
	 * http://127.0.0.1:8080/HiBotServer/api/v1/location/all
	 * 
	 * @return
	 */
	@GET
	@Path("/all")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Location> getAllLocation() {
		return new LocationDAO().queryAllLast();
	}

	/**
	 * <p>
	 * http://127.0.0.1:8080/HiBotServer/api/v1/location/id/1
	 * </p>
	 *
	 * @param id
	 * @return
	 */
	@GET
	@Path("/id/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Location getLocationById(@PathParam("id") String id) {
		return new LocationDAO().queryLast(id);
	}
	
	/**
	 * http://127.0.0.1:8080/HiBotServer/api/v1/location/list/id/1
	 * @param id
	 * @return
	 */
	@GET
	@Path("/list/id/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Location> getLocationListById(@PathParam("id") String id) {
		return new LocationDAO().queryAllById(id);
	}
	
	

	/**
	 * <p>
	 * http://127.0.0.1:8080/HiBotServer/api/v1/location/upload/id/3/latitude/40.22/longitude/119.4
	 * </p>
	 *
	 * @param id
	 * @return
	 */
	@GET
	@Path("/upload/id/{id}/latitude/{latitude}/longitude/{longitude}")
	@Produces(MediaType.APPLICATION_JSON)
	public ResultCode uploadLocation(@PathParam("id") String id,
			@PathParam("latitude") double latitude,
			@PathParam("longitude") double longitude) {

		Location loc = new Location();
		loc.clientId = id;
		loc.latitude = latitude;
		loc.longitude = longitude;
		
		return new LocationDAO().insertClient(loc)? new ResultCode(1):new ResultCode(0);
	}
	
}
