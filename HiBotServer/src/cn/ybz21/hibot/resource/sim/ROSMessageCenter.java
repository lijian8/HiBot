package cn.ybz21.hibot.resource.sim;

import java.util.HashMap;
import java.util.Map;

import ros.RosBridge;

public class ROSMessageCenter {

	private static Map<String, RosBridge> map = new HashMap<String, RosBridge>();

	public static RosBridge getBridge(String url) {
		RosBridge bridge = map.get(url);
		if (bridge == null) {
			bridge = new RosBridge();
			bridge.connect(url, true);
			map.put(url, bridge);
//			System.out.println("1");
		}else {
//			System.out.println("2");
		}
		return bridge;
	}
}
