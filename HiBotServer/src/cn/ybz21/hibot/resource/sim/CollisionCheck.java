package cn.ybz21.hibot.resource.sim;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import cn.ybz21.hibot.util.MessageCenter;
import cn.ybz21.hibot.util.StaticValues;
import ros.Publisher;
import ros.RosBridge;
import ros.msgs.std_msgs.PrimitiveMsg;

public class CollisionCheck {
	private static CollisionCheck cc;
	private boolean flag = false;
	private String url;
	private RosBridge bridge;
	private Publisher pub;

	private CollisionCheck() {
		// String url="ws://192.168.0.121:9090";
		url = StaticValues.URL_SIM;
		bridge = ROSMessageCenter.getBridge(url);
		pub = new Publisher("/uav_control", "std_msgs/String", bridge);
	}

	public static CollisionCheck getInstance() {
		if (cc == null) {
			cc = new CollisionCheck();
		}
		return cc;
	}

	public boolean isFlag() {
		return flag;
	}

	public void setFlag(boolean flag) {
		this.flag = flag;
	}

	public void stop() {
		flag = false;
	}

	public void start() {
		flag = true;
		while (flag) {
			check();
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public void check() {

		List<UavData> list = SimulationData.uavList;
		int size = list.size();
		for (UavData d : list)
			d.safeDistance = d.speed * 2 + 1.0;

		UavData datas[] = gen(list);
		List<TwoUav> dangerCpList = new ArrayList<>();
		Set<UavData> dangerSet = new HashSet<UavData>();
		for (int i = 0; i < size; i++) {
			for (int j = i + 1; j < size; j++) {
				if (UavData.isDanger(datas[i], datas[j])) {
					if (datas[i].z < datas[j].z)
						dangerCpList.add(new TwoUav(datas[i], datas[j]));
					else
						dangerCpList.add(new TwoUav(datas[j], datas[i]));
					dangerSet.add(datas[i]);
					dangerSet.add(datas[j]);
				}
			}
		}

		List<UavData> dangerList = new ArrayList<UavData>(dangerSet);
		Collections.sort(dangerList);

		// 1. 停止无人机
		for (int i = 0; i < dangerList.size(); i++) {
			System.out.println("danger:" + dangerList.get(i));
			String msg = new MessageCenter().genStopMesage(dangerList.get(i).roboNo, "UAV");
			pub.publish(new PrimitiveMsg<String>(msg));

		}

		// for()
		// 2 寻找高度
		for (int i = 0; i < dangerList.size() - 1; i++) {
			if (dangerList.get(i).z > 0.8) {
				double len = (size - i)  + 1;
				String msg = new MessageCenter().genUpMesage(dangerList.get(i).roboNo, len, "UAV");
				System.out.println("up msg:" + msg);
				pub.publish(new PrimitiveMsg<String>(msg));
				try {
					Thread.sleep(10000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		// 3 恢复原有运行路线
		for (int i = 0; i < dangerList.size(); i++) {
			// pub.publish(new
			// PrimitiveMsg<String>(SimulationData.lastCmd.get(dangerList.get(i).roboNo)));
			String msg = SimulationData.lastCmd.get(dangerList.get(i).roboNo);
			pub.publish(new PrimitiveMsg<String>(msg));
		}
		// 2. 寻找高度

	}

	private UavData[] gen(List<UavData> list) {
		int size = list.size();
		UavData[] datas = new UavData[size];
		int i = 0;
		for (UavData d : list) {
			datas[i] = d;
			i++;
		}
		return datas;
	}
}
