package cn.ybz21.hibot.resource.sim;

import java.io.Serializable;

public class UavData implements Serializable, Comparable<UavData> {
	public String roboNo;
	public double x, y, z, speed, safeDistance;
	
	
	public double targetZ;

	public static double getDistance(UavData d1, UavData d2) {
		return Math.sqrt(Math.pow((d1.x - d2.x), 2) + Math.pow((d1.y - d2.y), 2) + Math.pow((d1.z - d2.z), 2));
	}

	public static boolean isDanger(UavData d1, UavData d2) {
		double dis = getDistance(d1, d2);
		if (dis < Math.min(d1.safeDistance, d2.safeDistance))
			return true;
		return false;
	}

	@Override
	public String toString() {
		return "UavData [roboNo=" + roboNo + ", x=" + x + ", y=" + y + ", z=" + z + ", speed=" + speed
				+ ", safeDistance=" + safeDistance + "]";
	}

	@Override
	public int compareTo(UavData o) {
		if (o.z < this.z)
			return 1;
		else if (o.z > this.z)
			return -1;
		else
			return 0;
	}
}
