package cn.ybz21.hibot.resource;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import cn.ybz21.hibot.bean.ResultCode;
import cn.ybz21.hibot.bean.Task;
import cn.ybz21.hibot.dao.TaskDAO;
import cn.ybz21.hibot.util.MQTTPublisher;
import cn.ybz21.hibot.util.StaticValues;

import com.google.gson.Gson;

@Path("/task")
public class TaskResource {
	@GET
	@Produces(MediaType.TEXT_PLAIN)
	public String getDefaultTask() {
		return "任务管理";
	}

	/**
	 * http://localhost:8080/HiBotServer/api/v1/task/publish/taskIds/1/clientIds
	 * / 1,2,3
	 * 
	 * @param clientIds
	 * @param taskIds
	 * @return
	 */
	@GET
	@Path("/publish/taskIds/{taskIds}/clientIds/{clientIds}")
	@Produces(MediaType.APPLICATION_JSON)
	public ResultCode publishTask(@PathParam("clientIds") String clientIds,
			@PathParam("taskIds") String taskIds) {
		// System.out.println("clientIds:"+ clientIds + ",taskId:" + taskId);

		String[] client_ids = clientIds.split(",");
		String[] task_ids = taskIds.split(",");
		for (int i = 0; i < task_ids.length; i++) {
			String taskId = task_ids[i];
			System.out.println(taskId);
			Task task = getTaskByIdHelp(taskId);
			for (int j = 0; j < client_ids.length; j++) {
				String clientId = client_ids[j];
				MQTTPublisher.postMessage("addTask/" + clientId,
						new Gson().toJson(task));
				System.out.println("publish task " + taskId + " to client "
						+ clientId);
			}
		}

		String time = new SimpleDateFormat("yyyy-MM-dd,HH:mm:ss-SSS")
				.format(new Date());
		System.out.println("server publish task, time:" + time);
		ResultCode rCode = new ResultCode(0);
		return rCode;
	}

	/**
	 * http://localhost:8080/HiBotServer/api/v1/task/publishFile/clientId/1/
	 * filePath/myros.zip
	 * 
	 * @param clientId
	 * @param filePath
	 * @return
	 */
	@GET
	@Path("/publishFile/clientId/{clientId}/filePath/{filePath}")
	@Produces(MediaType.APPLICATION_JSON)
	public ResultCode publishFile(
			@PathParam(value = "clientId") String clientId,
			@PathParam(value = "filePath") String filePath) {
		System.out.println("clientId:" + clientId + ",filePath:" + filePath);
		filePath = "/home/ybz/myros.zip";
		ResultCode rCode = new ResultCode();
		// Task task = getTaskByIdHelp(taskId);
		MQTTPublisher.postFile("addTask2/" + clientId, filePath);
		return rCode;
	}

	private Task getTaskByIdHelp(String id) {
		Task task = new Task();
		task = new TaskDAO().queryTaskById(id);
		System.out.println(task.name);
		/*
		 * task.id = id; task.name = "10M";
		 */
		// task.programUrl = StaticValues.IP_HTTP_SERVER + "/task/task_" + id
		// + ".zip";
		task.programUrl = StaticValues.IP_HTTP_SERVER + task.programUrl;
		return task;
	}

	@GET
	@Path("/id/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Task getTaskById(@PathParam("id") String id) {

		return getTaskByIdHelp(id);
	}

	/**
	 * http://localhost:8080/HiBotServer/api/v1/task/finishTask/clientId/1
	 * 
	 * @param id
	 * @return
	 */
	@GET
	@Path("/finishTask/clientId/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public ResultCode finishTask(@PathParam("id") String id) {
		String time = new SimpleDateFormat("yyyy-MM-dd,HH:mm:ss-SSS")
				.format(new Date());
		System.out.println("id:" + id + ", finish task, time:" + time);
		ResultCode rCode = new ResultCode(0);
		return rCode;
	}

	/**
	 * <p>
	 * http://127.0.0.1:8080/HiBotServer/api/v1/task/list?startPage=1&pageSize=
	 * 100
	 * </p>
	 *
	 * @param startPage
	 * @param pageSize
	 * @return
	 */
	@GET
	@Path("/list")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Task> getTaskList(
			@QueryParam(value = "startPage") int startPage,
			@QueryParam(value = "pageSize") int pageSize) {
		if (pageSize == 0) {
			pageSize = 100;
			startPage = 1;
		}
		return new TaskDAO().queryTasks(startPage, pageSize);
	}

}