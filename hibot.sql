-- MySQL dump 10.13  Distrib 5.7.16, for Linux (x86_64)
--
-- Host: localhost    Database: hibot
-- ------------------------------------------------------
-- Server version	5.7.16-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `client`
--

DROP TABLE IF EXISTS `client`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `client` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `ip` varchar(45) DEFAULT NULL,
  `nodes` text,
  `state` int(11) DEFAULT '0',
  `idmac` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `client`
--

LOCK TABLES `client` WRITE;
/*!40000 ALTER TABLE `client` DISABLE KEYS */;
INSERT INTO `client` VALUES (1,'ybz','/192.168.1.129:37653','[{\"name\":\"/bumper2pointcloud\",\"clientIdMac\":\"1\"},{\"name\":\"/cmd_vel_mux\",\"clientIdMac\":\"1\"},{\"name\":\"/diagnostic_aggregator\",\"clientIdMac\":\"1\"},{\"name\":\"/mobile_base\",\"clientIdMac\":\"1\"},{\"name\":\"/mobile_base_nodelet_manager\",\"clientIdMac\":\"1\"},{\"name\":\"/robot_state_publisher\",\"clientIdMac\":\"1\"},{\"name\":\"/rosout\",\"clientIdMac\":\"1\"},{\"name\":\"/rqt_gui_py_node_5941\",\"clientIdMac\":\"1\"}]',0,'1'),(2,'sdx','/192.168.1.103:37826','[]',0,'client/-543347905'),(3,'weijing','/192.168.1.143:37928','[]',1,'client/-543347905'),(4,'Darrenchan','/192.168.1.84:37956','[]',0,'client/-543347905'),(5,'tzh','/192.168.1.214:38376','[]',1,'client_-543347905'),(6,'WangR','/192.168.1.111:38382','[]',1,'client/-543347905'),(7,'cyy','/192.168.1.36:383231','[]',0,'client/-543347905');
/*!40000 ALTER TABLE `client` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-04-15 19:01:46
