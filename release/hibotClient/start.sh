#!/bin/bash
# Set JavaHome if it exists
if [ -f "${JAVA_HOME}/bin/java" ]; then 
   JAVA=${JAVA_HOME}/bin/java
else
   JAVA=java
fi
export JAVA

export CONFIG_DIR=$PWD
echo   my configurePath:  $CONFIG_DIR
java -DCONFIG_DIR=$CONFIG_DIR -jar client.jar 

