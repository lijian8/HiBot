package cn.ybz21.hibot.bean;

import java.io.Serializable;

/**
 * 
 * @author ybz
 * @email bingzheng_yan@163.com
 * @time 2016年12月8日
 */
public class Node implements Serializable{

	private static final long serialVersionUID = -6732531353049171162L;
	public String name;
	public String clientIdMac;	
	/**
	 * 
	 * @param clientId
	 * @param name
	 */
	public Node(String clientId,String name) {
		super();
		this.name = name;
		this.clientIdMac=clientId;
	}
	public Node() {
		
	}
}
