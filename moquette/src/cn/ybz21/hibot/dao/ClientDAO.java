package cn.ybz21.hibot.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;

import cn.ybz21.hibot.bean.Client;
import cn.ybz21.hibot.db.DBHelper;

public class ClientDAO {
	private DBHelper db;
	private String NAME_TABLE = "client";

	public ClientDAO() {
		db = new DBHelper();
	}

	/**
	 * 
	 * @param client
	 * @return
	 */
	public boolean insertClient(Client client) {
		boolean flag = false;
		String sql = "insert into " + NAME_TABLE
				+ " (name,ip,nodes,state,idmac) values(?,?,?,?,?)";
		Connection conn = db.getConnection();
		PreparedStatement pstmt = null;
		try {
			pstmt = conn.prepareStatement(sql);
			int index = 1;
			pstmt.setObject(index++, client.name);
			pstmt.setObject(index++, client.ip);
			pstmt.setObject(index++, new Gson().toJson(client.nodes));
			pstmt.setObject(index++, client.state);
			pstmt.setObject(index++, client.idMac);
			int i = pstmt.executeUpdate();
			if (i > 0)
				flag = true;
			else
				flag = false;
		} catch (SQLException e) {
			e.printStackTrace();
			flag = false;
		}
		db.Close(conn, pstmt, null);
		return flag;
	}

	/**
	 * 
	 * @param client
	 * @return
	 */
	public boolean updateClient(Client client) {
		boolean flag = false;
		String sql = "update  " + NAME_TABLE				
				+ "  set name=?,ip=?,nodes=?,state=?  where id=?";
		Connection conn = db.getConnection();
		PreparedStatement pstmt = null;
		try {
			pstmt = conn.prepareStatement(sql);
			int index = 1;
			pstmt.setObject(index++, client.name);
			pstmt.setObject(index++, client.ip);
			pstmt.setObject(index++, new Gson().toJson(client.nodes));
			pstmt.setObject(index++, client.state);
			pstmt.setObject(index++, client.id);
			int i = pstmt.executeUpdate();
			if (i > 0)
				flag = true;
			else
				flag = false;
		} catch (SQLException e) {
			e.printStackTrace();
			flag = false;
		}
		db.Close(conn, pstmt, null);
		return flag;
	}

	/**
	 * 
	 * @param id
	 * @return
	 */
	public Client queryClientById(String id) {
		Client tempClient = null;
		String sql = "select * from  " + NAME_TABLE + "  where id=?";
		Connection conn = db.getConnection();
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			pstmt = conn.prepareStatement(sql);
			int index = 1;
			pstmt.setObject(index++, id);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				tempClient = new Client(rs);
			}
		} catch (SQLException e) {
			e.printStackTrace();

		}
		db.Close(conn, pstmt, rs);
		return tempClient;
	}

	/**
	 * 
	 * @param id
	 * @return
	 */
	public Client queryClientByIdMac(String id) {
		Client tempClient = null;
		String sql = "select * from  " + NAME_TABLE + "  where idmac=?";
		Connection conn = db.getConnection();
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			pstmt = conn.prepareStatement(sql);
			int index = 1;
			pstmt.setObject(index++, id);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				tempClient = new Client(rs);
			}
		} catch (SQLException e) {
			e.printStackTrace();

		}
		db.Close(conn, pstmt, rs);
		return tempClient;
	}

	/**
	 * 
	 * @param startPage
	 * @param pageSize
	 * @return
	 */
	public List<Client> queryAllClient(int startPage, int pageSize) {
		int pageIndex = (startPage - 1) * pageSize;
		List<Client> Clients = new ArrayList<Client>();
		String sql = "select * from " + NAME_TABLE + " limit ?,?";
		Connection conn = db.getConnection();
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			pstmt = conn.prepareStatement(sql);
			int index = 1;
			pstmt.setInt(index++, pageIndex);
			pstmt.setInt(index++, pageSize);
			rs = pstmt.executeQuery();
			while (rs.next()) {

				Client tempClient = new Client(rs);
				Clients.add(tempClient);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		db.Close(conn, pstmt, rs);
		return Clients;
	}
}
