package cn.ybz21.hibot.service;

import io.moquette.proto.messages.ConnectMessage;
import io.netty.channel.Channel;
import cn.ybz21.hibot.bean.Client;
import cn.ybz21.hibot.dao.ClientDAO;

public class ClientService {

	public void saveLinkClient(Channel channel, ConnectMessage msg) {
		
		Client client = new Client();
		client.ip=channel.remoteAddress().toString();
		client.idMac=msg.getClientID();
		client.name=msg.getUsername();
		client.state=Client.STATE_ONLINE;
		saveLinkClient(client);
		System.out.println();
	}

	public void deletLinkClient(String clientId) {
		Client client = new Client();
		client.idMac=clientId;
		client.state=Client.STATE_OFFLINE;
		ClientDAO dao = new ClientDAO();
		Client temp = dao.queryClientByIdMac(client.idMac);
		if (temp != null) {
			temp.state = Client.STATE_OFFLINE;
			dao.updateClient(temp);
		}
	}

	private void saveLinkClient(Client c) {
		ClientDAO dao = new ClientDAO();
		Client temp = dao.queryClientByIdMac(c.idMac);
		c.state = Client.STATE_ONLINE;
		if (temp == null) {
			dao.insertClient(c);
		} else {
			dao.updateClient(c);
		}

	}

	
}
