package cn.ybz21.hibot.ui;

import java.io.IOException;

import javax.swing.JFileChooser;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;
import org.eclipse.swt.widgets.Text;
import org.eclipse.wb.swt.SWTResourceManager;


import cn.ybz21.hibot.main.Main;
//import cn.ybz21.hibot.Main;
import cn.ybz21.hibot.util.GetMacAddress;
import cn.ybz21.hibot.util.StaticValues;
import cn.ybz21.hibot.util.shell.SudoExecutor;

public class SettingUI {

	protected Shell shlHibotStartSetting;
	private Text serverIP_1;
	private Text serverIP_2;
	private Text serverIP_3;
	private Text serverIP_4;
	private Text serverPort;
	private Text rootPwd;
	private Text pathWork;
	private Text pathRos;
	private Text macAddress;
	private String MACAddress;

	/**
	 * Launch the application.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			SettingUI window = new SettingUI();
			window.open();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Open the window.
	 */
	public void open() {
		Display display = Display.getDefault();
		createContents();
		shlHibotStartSetting.open();
		shlHibotStartSetting.layout();
		while (!shlHibotStartSetting.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
	}

	/**
	 * Create contents of the window.
	 */
	protected void createContents() {
		shlHibotStartSetting = new Shell();
		shlHibotStartSetting.setSize(883, 544);
		shlHibotStartSetting.setText("HiBot Start Setting");

		Composite composite = new Composite(shlHibotStartSetting, SWT.NONE);
		composite.setBounds(26, 33, 411, 251);

		TabFolder tabFolder = new TabFolder(composite, SWT.NONE);
		tabFolder.setBounds(0, 0, 411, 251);

		TabItem tbtmParameterConfiguration = new TabItem(tabFolder, SWT.NONE);
		tbtmParameterConfiguration.setText("Parameter Configuration");

		Group group = new Group(tabFolder, SWT.NONE);
		tbtmParameterConfiguration.setControl(group);

		Label lblNewLabel = new Label(group, SWT.CENTER);
		lblNewLabel.setAlignment(SWT.RIGHT);
		lblNewLabel.setBounds(26, 27, 71, 23);
		lblNewLabel.setText("IP SERVER");

		serverIP_1 = new Text(group, SWT.BORDER | SWT.CENTER);
		serverIP_1.setBounds(104, 23, 45, 27);

		Label label = new Label(group, SWT.NONE);
		label.setBounds(155, 27, 4, 23);
		label.setText(".");

		serverIP_2 = new Text(group, SWT.BORDER | SWT.CENTER);
		serverIP_2.setBounds(165, 23, 45, 27);

		Label label_1 = new Label(group, SWT.NONE);
		label_1.setText(".");
		label_1.setBounds(216, 27, 4, 23);

		serverIP_3 = new Text(group, SWT.BORDER | SWT.CENTER);
		serverIP_3.setBounds(226, 23, 45, 27);

		Label label_2 = new Label(group, SWT.NONE);
		label_2.setText(".");
		label_2.setBounds(277, 27, 4, 23);

		serverIP_4 = new Text(group, SWT.BORDER | SWT.CENTER);
		serverIP_4.setBounds(287, 23, 45, 27);

		Label label_3 = new Label(group, SWT.NONE);
		label_3.setText(":");
		label_3.setBounds(338, 27, 4, 23);

		serverPort = new Text(group, SWT.BORDER | SWT.CENTER);
		serverPort.setBounds(348, 23, 45, 27);

		Label lblPassword = new Label(group, SWT.CENTER);
		lblPassword.setText("PASSWORD");
		lblPassword.setAlignment(SWT.RIGHT);
		lblPassword.setBounds(16, 71, 81, 23);

		rootPwd = new Text(group, SWT.BORDER);
		rootPwd.setBounds(104, 67, 289, 27);

		Label lblWorkspace = new Label(group, SWT.CENTER);
		lblWorkspace.setText("WORKSPACE");
		lblWorkspace.setAlignment(SWT.RIGHT);
		lblWorkspace.setBounds(10, 120, 87, 23);

		pathWork = new Text(group, SWT.BORDER);
		pathWork.setBounds(104, 116, 239, 27);

		Label lblPathRos = new Label(group, SWT.RIGHT);
		lblPathRos.setText("PATH ROS");
		lblPathRos.setBounds(16, 169, 81, 23);

		pathRos = new Text(group, SWT.BORDER);
		pathRos.setBounds(104, 165, 238, 27);

		Button chooseWorkspace = new Button(group, SWT.NONE);
		chooseWorkspace.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				JFileChooser fileChooser = new JFileChooser("D:\\");
				fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
				int returnVal = fileChooser.showOpenDialog(fileChooser);
				if (returnVal == JFileChooser.APPROVE_OPTION) {
					String filePath = fileChooser.getSelectedFile()
							.getAbsolutePath();// 这个就是你选择的文件夹的路径
					pathWork.setText(filePath);
				}
			}
		});
		chooseWorkspace.setBounds(348, 114, 45, 29);
		chooseWorkspace.setText("...");

		Button choosePathRos = new Button(group, SWT.NONE);
		choosePathRos.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				JFileChooser fileChooser = new JFileChooser("D:\\");
				fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
				int returnVal = fileChooser.showOpenDialog(fileChooser);
				if (returnVal == JFileChooser.APPROVE_OPTION) {
					String filePath = fileChooser.getSelectedFile()
							.getAbsolutePath();// 这个就是你选择的文件夹的路径
					pathRos.setText(filePath);
				}
			}
		});
		choosePathRos.setText("...");
		choosePathRos.setBounds(348, 163, 45, 29);

		Composite composite_1 = new Composite(shlHibotStartSetting, SWT.NONE);
		composite_1.setBounds(459, 33, 396, 251);

		TabFolder tabFolder_1 = new TabFolder(composite_1, SWT.NONE);
		tabFolder_1.setBounds(0, 0, 396, 251);

		TabItem tbtmOperationConfiguration = new TabItem(tabFolder_1, SWT.NONE);
		tbtmOperationConfiguration.setText("Operation Configuration");

		Group grpOperationConfiguration = new Group(tabFolder_1, SWT.NONE);
		tbtmOperationConfiguration.setControl(grpOperationConfiguration);

		Label lblNewLabel_2 = new Label(grpOperationConfiguration, SWT.BORDER
				| SWT.WRAP);
		lblNewLabel_2.setFont(SWTResourceManager.getFont("Ubuntu", 13,
				SWT.NORMAL));
		lblNewLabel_2.setForeground(SWTResourceManager
				.getColor(SWT.COLOR_WIDGET_FOREGROUND));
		lblNewLabel_2.setBounds(10, 10, 357, 50);
		lblNewLabel_2
				.setText("Since SHELL needs bash authority, do you want to change to bash mode?");

		Label lblYouNeedTo = new Label(grpOperationConfiguration, SWT.BORDER
				| SWT.WRAP | SWT.CENTER);
		lblYouNeedTo.setAlignment(SWT.LEFT);
		lblYouNeedTo.setText("You need fill the PASSWORD blank first!");
		lblYouNeedTo.setForeground(SWTResourceManager.getColor(SWT.COLOR_RED));
		lblYouNeedTo.setFont(SWTResourceManager.getFont("Ubuntu", 13,
				SWT.NORMAL));
		lblYouNeedTo.setBounds(10, 150, 357, 43);

		// 将shell的执行模式由dash变为bash
		Button changeBash = new Button(grpOperationConfiguration, SWT.NONE);
		changeBash.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				StaticValues.PWD = rootPwd.getText();
				try {
					SudoExecutor.run(SudoExecutor.buildCommands(
							"sudo ln -fs /bin/bash /bin/sh", StaticValues.PWD));
				} catch (IOException | InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		changeBash.setBounds(63, 90, 249, 29);
		changeBash.setText("Change to BASH");

		Composite composite_2 = new Composite(shlHibotStartSetting, SWT.NONE);
		composite_2.setBounds(26, 331, 829, 103);

		TabFolder tabFolder_2 = new TabFolder(composite_2, SWT.NONE);
		tabFolder_2.setLocation(0, 0);
		tabFolder_2.setSize(829, 103);

		TabItem tbtmMacAddress = new TabItem(tabFolder_2, SWT.NONE);
		tbtmMacAddress.setText("MAC Address");

		Group group_1 = new Group(tabFolder_2, SWT.NONE);
		tbtmMacAddress.setControl(group_1);

		// 设置MAC地址自动获取
		Label lblNewLabel_3 = new Label(group_1, SWT.NONE);
		lblNewLabel_3.setBounds(123, 20, 155, 23);
		lblNewLabel_3.setText("Your MAC address is : ");

		macAddress = new Text(group_1, SWT.BORDER | SWT.READ_ONLY);
		macAddress.setBackground(SWTResourceManager
				.getColor(SWT.COLOR_TITLE_INACTIVE_BACKGROUND));
		macAddress.setBounds(291, 20, 348, 27);
		MACAddress = GetMacAddress.getLocMac();
		macAddress.setText(MACAddress);

		// 开始按钮触发事件
		Button start = new Button(shlHibotStartSetting, SWT.NONE);
		start.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {

				// 设置StaticValue.java中的变量
				StaticValues.IP_SERVER = serverIP_1.getText() + "."
						+ serverIP_2.getText() + "." + serverIP_3.getText()
						+ "." + serverIP_4.getText() + ":"
						+ serverPort.getText();
				
				StaticValues.ID_CLIENT = MACAddress.hashCode()+"";
				StaticValues.PWD = rootPwd.getText();
				StaticValues.PATH_WORK = pathWork.getText();
				StaticValues.PATH_ROS = pathRos.getText();

				Main.start();
			}
		});
		start.setBounds(202, 457, 95, 29);
		start.setText("Start");

		Button cancel = new Button(shlHibotStartSetting, SWT.NONE);
		cancel.setBounds(573, 457, 95, 29);
		cancel.setText("Cancel");

	}
}
