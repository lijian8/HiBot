package cn.ybz21.hibot.topic_process_center.node;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import cn.ybz21.hibot.topic_process_center.base.RPCBaseCenter;
import cn.ybz21.hibot.util.StaticValues;
import cn.ybz21.hibot.util.shell.CmdExec;

public class NodeInfoCenter extends RPCBaseCenter {

	@Override
	public String getAddr() {
		return StaticValues.URL_UP_NODE_INFO;
	}

	@Override
	public String execShell() {
		String shell = "rosnode info rosout";
		String str = new CmdExec().execCmd4Str(shell);
		System.out.println("rosnode result:\n" + str + "\n end result");
		return str;
	}

	@Override
	public List<NameValuePair> process(String str) {
		// for()
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("data", str));
		return params;
	}

//	public static void main(String args[]) {
//		new NodeInfoCenter().doAction(null, null);
//	}

}
