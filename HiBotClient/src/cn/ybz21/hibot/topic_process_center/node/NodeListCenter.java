package cn.ybz21.hibot.topic_process_center.node;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import cn.ybz21.hibot.bean.Node;
import cn.ybz21.hibot.topic_process_center.base.RPCBaseCenter;
import cn.ybz21.hibot.util.StaticValues;
import cn.ybz21.hibot.util.shell.CmdExec;

import com.google.gson.Gson;

/**
 * 获取已经执行的node
 * 
 * @author ybz
 * @email bingzheng_yan@163.com
 * @time 2016年12月8日
 */
public class NodeListCenter extends RPCBaseCenter {
//	public static void main(String args[]) {
//		new NodeListCenter().doAction(null, null);
//	}

	@Override
	public String getAddr() {
		return StaticValues.URL_UP_NODE_LIST;
	}

	@Override
	public String execShell() {
		String shell = "rosnode list";
		String strs = new CmdExec().execCmd4Str(shell);
		return strs;
	}

	@Override
	public List<NameValuePair> process(String str) {
		List<Node> nodes = new ArrayList<Node>();
		String strs[] = str.split("\n");
		for (int i = 0; i < strs.length; i++) {
			Node node = new Node(StaticValues.ID_CLIENT, strs[i]);
			nodes.add(node);
			System.out.println(node);
		}
		String jsonString = new Gson().toJson(nodes);
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("data", jsonString));
		return params;
	}
}
