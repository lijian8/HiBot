package cn.ybz21.hibot.topic_process_center.base;

import com.ibm.micro.client.mqttv3.MqttMessage;
import com.ibm.micro.client.mqttv3.MqttTopic;
/**
 * 
 * @author ybz
 * @email bingzheng_yan@163.com
 * @time 2016年12月8日
 */
public interface BaseCenter {
 public void doAction(MqttTopic topic, MqttMessage message);
}
