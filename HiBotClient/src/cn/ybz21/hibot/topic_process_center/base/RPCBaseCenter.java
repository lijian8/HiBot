package cn.ybz21.hibot.topic_process_center.base;

import java.util.List;

import org.apache.http.NameValuePair;

import cn.ybz21.hibot.net.NetCenter;

import com.ibm.micro.client.mqttv3.MqttMessage;
import com.ibm.micro.client.mqttv3.MqttTopic;

/**
 * 
 * @author ybz
 * @email bingzheng_yan@163.com
 * @time 2016年12月8日
 */
public abstract class RPCBaseCenter implements BaseCenter {
	@Override
	public void doAction(MqttTopic topic, MqttMessage message) {
		String clazz = this.getClass().getName();
		System.out.println(clazz+" start");
		String strs = execShell();
		List<NameValuePair> params = process(strs);
		String addr = getAddr();
		sendResult(addr, params);
		System.out.println(clazz+" end");
	}

	public abstract String getAddr();

	public abstract String execShell();

	public abstract List<NameValuePair> process(String strs);

	public void sendResult(String addr, List<NameValuePair> params) {
		String result = new NetCenter().httpPost(addr, params);
		System.out.println("result:" + result);
	}
}
