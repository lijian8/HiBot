package cn.ybz21.hibot.topic_process_center.app;

import java.util.HashMap;
import java.util.Map;

public class AppStorage {
	private static Map<String, Process> map = new HashMap<String, Process>();

	protected static void addAppProcess(String id, Process process) {
		map.put(id, process);
	}

	protected static Process getProcess(String id) {
		return map.get(id);
	}
}
