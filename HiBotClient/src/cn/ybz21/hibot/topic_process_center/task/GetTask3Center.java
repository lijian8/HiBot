package cn.ybz21.hibot.topic_process_center.task;

import java.io.IOException;

import cn.ybz21.hibot.bean.Task;
import cn.ybz21.hibot.net.NetCenter;
import cn.ybz21.hibot.util.DownloadUtil;
import cn.ybz21.hibot.util.StaticValues;

import com.google.gson.Gson;

/**
 * 
 * @author ybz
 * @email bingzheng_yan@163.com
 * @time 2016年12月9日
 */
public class GetTask3Center {
//	public static void main(String args[]) {
//		new GetTask3Center().doAction();
//	}

	public void doAction() {
		while (true) {
			try {
				String str = new NetCenter()
						.httpGet("http://localhost:8080/HiBotServer/rest/polling/id/"
								+ StaticValues.ID_CLIENT);
				Task task = new Gson().fromJson(str, Task.class);
				if (task.id.equals("-1")) {
					// 没有新任务
					System.out.println("no new task");
				} else {
					// 有新任务
					System.out.println("yes new task");
					new MT(task).start();
				}
				Thread.currentThread();
				Thread.sleep(5000);
			} catch (IOException | InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	class MT extends Thread {
		Task task;
		public MT(Task task){
			this.task=task;
		}
		@Override
		public void run() {
			super.run();
			try {
				DownloadUtil.downloadFile(task.programUrl, StaticValues.PATH_ZIP,
						task.name + ".zip");
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
