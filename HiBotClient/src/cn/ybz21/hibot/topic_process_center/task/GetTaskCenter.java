package cn.ybz21.hibot.topic_process_center.task;

import java.io.IOException;

import cn.ybz21.hibot.bean.Task;
import cn.ybz21.hibot.net.NetCenter;
import cn.ybz21.hibot.topic_process_center.base.BaseCenter;
import cn.ybz21.hibot.util.DownloadUtil;
import cn.ybz21.hibot.util.FileUtil;
import cn.ybz21.hibot.util.StaticValues;
import cn.ybz21.hibot.util.shell.ShellExecutor;
import cn.ybz21.hibot.util.shell.SudoExecutor;

import com.google.gson.Gson;
import com.ibm.micro.client.mqttv3.MqttMessage;
import com.ibm.micro.client.mqttv3.MqttTopic;

/**
 * 推送任务处理中心
 * 
 * @author ybz
 * @email bingzheng_yan@163.com
 * @time 2016年11月17日
 */
public class GetTaskCenter implements BaseCenter {

//	public static void main(String[] args) {
//		Task task = new Task();
//		task.name = "myros";
//		task.id = "1";
//		new GetTaskCenter().taskProcess(task);
//	}

	@Override
	public void doAction(MqttTopic topic, MqttMessage message) {
		System.out.println(message.toString());
		Gson gson = new Gson();
		Task task = (Task) gson.fromJson(message.toString(), Task.class);
		taskProcess(task);
	}

	/**
	 * 
	 * @param task
	 */
	private void taskProcess(Task task) {
		System.out.println("******************************************");
		System.out.println("start process");
		System.out.println("start download");
		// step 1 下载
		try {
			DownloadUtil.downloadFile(task.programUrl, StaticValues.PATH_ZIP,
					task.name + ".zip");
		} catch (IOException e) {
			e.printStackTrace();
			System.err.println("download error！");
		}
		// step 2 下载完成
		onFinish(task);
		System.out.println("finish download");
		// System.out.println("finish download");
		// System.out.println("start unzip");
		// // step 3 解压
		// ZipUtil.unzip(StaticValues.PATH_ZIP + "/" + task.name + ".zip",
		// StaticValues.PATH_UNZIP);
		// System.out.println("finish unzip");
		// System.out.println("start copy");
		// // step 4 复制
		// FileUtil.copyFolder(StaticValues.PATH_UNZIP + "/" + task.name,
		// StaticValues.PATH_ROS_SRC + "/" + task.name);
		// System.out.println("finish copy");
		// // step 5 生成shell
		// genShells(task);
		// // step 6 运行shell
		// System.out.println("start run");
		// runShells(task);

	}

	private void onFinish(Task task) {
		String urlString = StaticValues.URL_FINISH_DOWNLOAD_TASK + "/"
				+ StaticValues.ID_CLIENT;
		String result = "";
		try {
			result = new NetCenter().httpGet(urlString);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("result:" + result);
	}

	private void runShells(Task task) {
		SudoExecutor exe = new SudoExecutor();
		try {
			exe.chmod(StaticValues.PATH_ROS_SRC);
			exe.chmod(StaticValues.PATH_SHELL + "/" + task.name + ".sh");
		} catch (IOException | InterruptedException e) {
			e.printStackTrace();
			System.err.println("error in add chmod");
		}
		ShellExecutor.execLocal("sh " + StaticValues.PATH_SHELL + "/"
				+ task.name + ".sh",false);
	}

	private void genShells(Task task) {
		// TODO 生成run ros的shell
		String str = "#!/bin/bash";
		String str1 = "cd " + StaticValues.PATH_ROS;
		String str2 = "catkin_make";
		String str3 = "source ./devel/setup.bash";

		String str4 = "rosrun " + task.name + " talker.py";
		// String str5 = "rosrun "+task.name+" listener.py";
		String strs[] = { str, str1, str2, str3, str4 };
		FileUtil.writeFile(strs, StaticValues.PATH_SHELL, task.name + ".sh");
	}

}