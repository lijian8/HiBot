package cn.ybz21.hibot.topic_process_center.task;

import java.io.IOException;

import cn.ybz21.hibot.topic_process_center.base.BaseCenter;
import cn.ybz21.hibot.util.FileUtil;
import cn.ybz21.hibot.util.StaticValues;
import cn.ybz21.hibot.util.ZipUtil;

import com.ibm.micro.client.mqttv3.MqttException;
import com.ibm.micro.client.mqttv3.MqttMessage;
import com.ibm.micro.client.mqttv3.MqttTopic;

/**
 * 
 * @author ybz
 * @email bingzheng_yan@163.com
 * @time 2016年12月9日
 */
public class GetTask2Center implements BaseCenter {
//	public static void main(String args[]) {
//		ZipUtil.unzip(StaticValues.PATH_ZIP + "/temp.zip",
//				StaticValues.PATH_UNZIP);
//	}

	@Override
	public void doAction(MqttTopic topic, MqttMessage message) {
		System.out.println("GetTask2Center");
		//step 1 接收推送
		downloadFile(message);
		//step 2 解压文件
		ZipUtil.unzip(StaticValues.PATH_ZIP + "/temp.zip",
				StaticValues.PATH_UNZIP);
		//
	}
//void unzip(
	void downloadFile(MqttMessage message) {
		try {
			byte[] bs = message.getPayload();
			FileUtil.createFile(StaticValues.PATH_ZIP + "/temp.zip", bs);
		} catch (MqttException | IOException e) {
			System.out.println("GetTask2Center error");
			e.printStackTrace();
		}
	}
}
