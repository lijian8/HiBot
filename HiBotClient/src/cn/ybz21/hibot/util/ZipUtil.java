package cn.ybz21.hibot.util;

import java.io.File;

import org.apache.tools.ant.Project;
import org.apache.tools.ant.taskdefs.Expand;
import org.apache.tools.ant.taskdefs.Zip;
import org.apache.tools.ant.types.FileSet;

/**
 * 压缩文件的通用工具类-采用ant中的org.apache.tools.ant.taskdefs.Zip来实现，更加简单
 * @author wj
 * @email
 * @time 2016年11月15日
 */
public class ZipUtil {

	/**
	 * 执行压缩操作
	 * @param srcPathName
	 *            需要被压缩的文件/文件夹
	 * @param finalFile
	 */
	public static void zip(String srcPathName, String finalFile) {
		File srcdir = new File(srcPathName);
		File zipFile = new File(finalFile);
		if (!srcdir.exists()) {
			throw new RuntimeException(srcPathName + "not exist！");
		}

		Project prj = new Project();
		Zip zip = new Zip();
		zip.setProject(prj);
		zip.setDestFile(zipFile);
		FileSet fileSet = new FileSet();
		fileSet.setProject(prj);
		fileSet.setDir(srcdir);
		zip.addFileset(fileSet);
		zip.execute();
	}

	/**
	 * 解压文件
	 * 
	 * @param zipFilePath
	 * @param dir
	 */
	public static void unzip( String zipFilePath,String dir) {
		Expand expand = new Expand();
		expand.setDest(new File(dir));
		expand.setSrc(new File(zipFilePath));
		Project p = new Project();
		expand.setProject(p);
		expand.setEncoding("GBK");
		expand.execute();
	}

//	public static void main(String[] args) {
//		zip("/home/ybz/git", "/home/ybz/git.zip");
//		unzip("/home/ybz/git.zip","/home/ybz/myzip");
//	}
}