package cn.ybz21.hibot.util;
import java.net.NetworkInterface;
import java.util.Enumeration;

public class GetMacAddress {
			
	public static String getLocMac() {
		
		StringBuilder builder = new StringBuilder();
		try {
			Enumeration<NetworkInterface> el = NetworkInterface
					.getNetworkInterfaces();
			
			while (el.hasMoreElements()) {
				byte[] mac = el.nextElement().getHardwareAddress();
				if (mac == null) {
					continue;
				}
				if (builder.length() > 0) {
					builder.append(",");
				}
				for (byte b : mac) {

					// convert to hex string.
					String hex = Integer.toHexString(0xff & b).toUpperCase();
					if (hex.length() == 1) {
						hex = "0" + hex;
					}
					builder.append(hex);
					builder.append("-");
				}
				builder.deleteCharAt(builder.length() - 1);
			}

		} catch (Exception exception) {
			exception.printStackTrace();
		}
		
		if (builder.length() == 0) {
//			System.out.println("Sorry, can't find your MAC Address.");
			return "Failure!";
		} else {
//			System.out.println("Your MAC Address is " + builder.toString());
			return builder.toString();
		}
	}
}
