package cn.ybz21.hibot.util;

import cn.ybz21.hibot.util.properties.PropertiesUtil;

public class StaticValues {

	public static final String PROPERTIES = "hibot.properties";
	public static String IP_SERVER = PropertiesUtil.getStringByKey("ip",PROPERTIES);
	public static String ID_CLIENT = PropertiesUtil.getStringByKey("clientId",PROPERTIES);
	public static String PWD = PropertiesUtil.getStringByKey("pwd", PROPERTIES);
	public static String PATH_WORK = PropertiesUtil.getStringByKey("pathUser",PROPERTIES);
	public static String PATH_ROS = PropertiesUtil.getStringByKey("pathROS",PROPERTIES);
	
	public static String DB_USER=PropertiesUtil.getStringByKey("dbUser",PROPERTIES);
	public static String DB_PWD=PropertiesUtil.getStringByKey("dbPwd",PROPERTIES);
	
	public static final String HTTP_SERVER = "http://" + IP_SERVER+ ":8080/HiBotServer";
	public static final String MQTT_SERVER = "tcp://" + IP_SERVER + ":1883";
	
	public static final String PATH_ZIP = PATH_WORK + "/hibot/zip";
	public static final String PATH_UNZIP = PATH_WORK + "/hibot/unzip";
	public static final String PATH_SHELL = PATH_WORK + "/hibot/shell";
	public static final String PATH_LOG = PATH_WORK + "/hibot/log";
	public static final String PATH_ROS_SRC = PATH_ROS + "/src";
	public static final String PATH_ROS_BASH = PATH_ROS + "/devel/setup.sh";
	public static final String[] PATHS = { PATH_ZIP, PATH_UNZIP, PATH_SHELL ,PATH_LOG};
	
	public static final String SHELL_STOP_APP="stopApp.sh";
	
	public static final String URL_UP_NODE_LIST = HTTP_SERVER+ "/api/v1/node/uploadNodeList";
	public static final String URL_UP_NODE_INFO = HTTP_SERVER+ "/api/v1/node/uploadNodeInfo";
	public static final String URL_FINISH_DOWNLOAD_TASK = HTTP_SERVER +"/api/v1/task/finishTask/clientId";
	public static final String URL_UPLOAD_APP_INFO = HTTP_SERVER + "/api/v1/app/monitor/uploadAppInfo";
	
	public static final String TABLE_APP="app_client";
}