package cn.ybz21.hibot.util;

public class StaticTopics {
	public final static String TOPIC_TASK_ADD = "addTask/"
			+ StaticValues.ID_CLIENT;

	public final static String TOPIC_TASK_ADD2 = "addTask2/"
			+ StaticValues.ID_CLIENT;

	public final static String TOPIC_TASK_LIST = "getTaskList/"
			+ StaticValues.ID_CLIENT;

	public final static String TOPIC_NODE_INFO = "getNodeInfo/"
			+ StaticValues.ID_CLIENT;

	public final static String TOPIC_APP_DOWNLOAD = "downloadApp/"
			+ StaticValues.ID_CLIENT;
	public final static String TOPIC_APP_START = "startApp/"
			+ StaticValues.ID_CLIENT;
	public final static String TOPIC_APP_STOP = "stopApp/"
			+ StaticValues.ID_CLIENT;

	public final static String[] topics = { TOPIC_TASK_ADD, TOPIC_TASK_ADD2,
			TOPIC_TASK_LIST, TOPIC_APP_DOWNLOAD, TOPIC_APP_START,
			TOPIC_APP_STOP };
}
