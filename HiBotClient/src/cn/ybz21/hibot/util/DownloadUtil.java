package cn.ybz21.hibot.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

/**
 * 文件下载工具
 * 
 * @author ybz
 * @email bingzheng_yan@163.com
 * @time 2016年11月15日
 */
public class DownloadUtil {
	/**
	 * 下载文件
	 * 
	 * @param urlPath
	 *            网络地址
	 * @param filePath
	 *            存储路径 例：/home/ybz/data
	 * @param filename
	 *            文件名称
	 * @throws IOException
	 */
	public static void downloadFile(String urlPath, String filePath,
			String filename) throws IOException {
		File dirFile = new File(filePath);
		if (!dirFile.exists()) {
			// 文件路径不存在时，自动创建目录
			dirFile.mkdirs();
		}

		URL theURL = new URL(urlPath);
		// 从服务器上获取图片并保存
		URLConnection connection = theURL.openConnection();
		InputStream in = connection.getInputStream();
		FileOutputStream os = new FileOutputStream(filePath + "/" + filename);
		byte[] buffer = new byte[4 * 1024];
		int read;
		while ((read = in.read(buffer)) > 0) {
			os.write(buffer, 0, read);
		}
		os.close();
		in.close();
	}

	// public static void main(String[] args) {
	// // 下面添加服务器的IP地址和端口，以及要下载的文件路径
	// String urlPath = "http://192.168.2.129:8080/HiBotServer/task/myros.zip";
	// String filePath = "/home/ybz/hibot/zip";
	// try {
	// downloadFile(urlPath, filePath,"a.zip");
	// } catch (IOException e) {
	// e.printStackTrace();
	// }
	// }
}
