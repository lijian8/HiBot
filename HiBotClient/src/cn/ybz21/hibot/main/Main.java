package cn.ybz21.hibot.main;

import java.io.File;

import cn.ybz21.hibot.util.StaticValues;

public class Main {
	//
	public static void start() {
		initFile();
		MQTTHelper.subscribeTopic();
	}

	private static void initFile() {
		for (int i = 0; i < StaticValues.PATHS.length; i++) {
			File file = new File(StaticValues.PATHS[i]);
			if (!file.exists()) {
				file.mkdirs();
			}
		}
	}

	public static void main(String[] args) {
		start();
	}
}
