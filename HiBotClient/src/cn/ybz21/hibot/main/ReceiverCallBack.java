package cn.ybz21.hibot.main;

import cn.ybz21.hibot.topic_process_center.app.AppDownloadCenter;
import cn.ybz21.hibot.topic_process_center.app.AppStartCenter;
import cn.ybz21.hibot.topic_process_center.app.AppStopCenter;
import cn.ybz21.hibot.topic_process_center.base.BaseCenter;
import cn.ybz21.hibot.topic_process_center.node.NodeInfoCenter;
import cn.ybz21.hibot.topic_process_center.node.NodeListCenter;
import cn.ybz21.hibot.topic_process_center.task.GetTask2Center;
import cn.ybz21.hibot.topic_process_center.task.GetTaskCenter;
import cn.ybz21.hibot.util.StaticTopics;

import com.ibm.micro.client.mqttv3.MqttCallback;
import com.ibm.micro.client.mqttv3.MqttDeliveryToken;
import com.ibm.micro.client.mqttv3.MqttMessage;
import com.ibm.micro.client.mqttv3.MqttTopic;

/**
 * 回调处理类 处理订阅的消息类
 * 
 * @author ybz
 * @email bingzheng_yan@163.com
 * @time 2016年11月14日
 */
public class ReceiverCallBack implements MqttCallback {
	/**
	 * 接收到信息的处理
	 */
	public void messageArrived(MqttTopic topic, MqttMessage message) {
		// System.out.println("MQTTSubsribe Receive Topic:" + topic.toString()
		// + ",msg:" + ":" + message.toString());
		String tp = topic.toString();
		if (tp.equals(StaticTopics.TOPIC_TASK_ADD)) {
			// 处理推送的任务
			BaseCenter center = new GetTaskCenter();
			center.doAction(topic, message);
		} else if (tp.equals(StaticTopics.TOPIC_TASK_LIST)) {
			// 查看任务列表
			System.out.println("查看列表");
			BaseCenter center = new NodeListCenter();
			center.doAction(topic, message);
		} else if (tp.equals(StaticTopics.TOPIC_TASK_ADD2)) {
			// 查看任务列表
			BaseCenter center = new GetTask2Center();
			center.doAction(topic, message);
			
		} else if (tp.equals(StaticTopics.TOPIC_APP_DOWNLOAD)) {
			BaseCenter center = new AppDownloadCenter();
			center.doAction(topic, message);
		} else if (tp.equals(StaticTopics.TOPIC_APP_START)) {
			BaseCenter center = new AppStartCenter();
			center.doAction(topic, message);
		}  else if (tp.equals(StaticTopics.TOPIC_APP_STOP)) {
			BaseCenter center = new AppStopCenter();
			center.doAction(topic, message);
		}else
			System.out.println("no hadler for topic:" + topic.getName());

	}

	public void connectionLost(Throwable cause) {
		System.out.println("connnetion lost");
	}

	public void deliveryComplete(MqttDeliveryToken token) {
		System.out.println("deliveryComplete");
	}
}
