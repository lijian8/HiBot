package cn.ybz21.hibot.bean;

import java.io.Serializable;

/**
 * 
 * @author ybz
 * @email bingzheng_yan@163.com
 * @time 2016年12月8日
 */
public class Node implements Serializable {

	private static final long serialVersionUID = -6732531353049171162L;
	public String name;
	public String clientIdMac;
	public String pid;
	public double cpu;
	public double memory;
	public String info;

	/**
	 * 
	 * @param clientId
	 * @param name
	 */
	public Node(String clientId, String name) {
		super();
		this.name = name;
		this.clientIdMac = clientId;
	}

	public Node() {

	}

	@Override
	public String toString() {
		return "Node [name=" + name + ", pid=" + pid + ", cpu=" + cpu
				+ ", memory=" + memory + "]";
	}

}
