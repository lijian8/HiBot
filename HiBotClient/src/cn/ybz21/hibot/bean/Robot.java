package cn.ybz21.hibot.bean;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class Robot {
	public String id;
	public String idMac;
	public String name;
	public String ip;
	public String type;
	public List<Node> nodes;
	public int state;

	public final static int STATE_ONLINE = 1;
	public final static int STATE_OFFLINE = 0;

	public Robot() {
		nodes = new ArrayList<Node>();
	}

	public Robot(ResultSet resultSet) throws SQLException {
		super();
		id = resultSet.getString("client.id");
		name = resultSet.getString("client.name");
		ip = resultSet.getString("client.ip");

		if (Math.random() > 0.5)
			type = "UAV";
		else
			type = "TurtleBot";
		
		String temp = resultSet.getString("client.nodes");
		nodes = new Gson().fromJson(temp, new TypeToken<List<Node>>() {
		}.getType());
		if (nodes == null)
			nodes = new ArrayList<Node>();
		idMac = resultSet.getString("client.idmac");
		state = resultSet.getInt("client.state");
	}
}
