package cn.ybz21.hibot.bean.ros;

import java.io.Serializable;

public class Topic implements Serializable{
	private static final long serialVersionUID = -7321491023475503574L;
	public String name;
	public String to;
	public String direction;
	public String transport;
}
