package cn.ybz21.hibot.bean;

import java.io.Serializable;

/**
 * 网络请求返回吗
 * @author ybz
 * @email bingzheng_yan@163.com
 * @time 2016年11月15日
 */
public class ResultCode implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 922139713231870697L;
	public int code;
}
