package cn.ybz21.hibot.bean;

import java.io.Serializable;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class App implements Serializable {

	private static final long serialVersionUID = 8529613703287077106L;
	public String id;
	public String name;
	public String md5;
	public String downloadLink;
	public String startCmd;
	public String stopCmd;
	public String clientId;
	public String version;
    //父进程
	public String pid;
	public boolean health;
	public List<Node> nodes;
	public double cpu;
	public double memory;
	public String describe;
	public AppState state;

	public App() {

	}

	public App(ResultSet rs) {
		try {
			id = rs.getString("id");
			name = rs.getString("name");
			md5 = rs.getString("md5");
			downloadLink = rs.getString("download_link");
			startCmd = rs.getString("start_cmd");
			stopCmd = rs.getString("stop_cmd");
			clientId = rs.getString("client_id");
			health = rs.getBoolean("health");
			cpu = rs.getDouble("cpu");
			memory = rs.getDouble("memory");
			describe = rs.getString("descr");
			pid=rs.getString("pid");
			version = rs.getString("version");
			String tmp = rs.getString("state");
			if (tmp != null) {
				if (tmp.equals(AppState.RUNNING.toString()))
					state = AppState.RUNNING;
				else
					state = AppState.STOP;
			}
			String nodestr = rs.getString("nodes");
			Gson gson = new Gson();
			nodes = gson.fromJson(nodestr, new TypeToken<List<Node>>() {
			}.getType());
			if (nodes == null)
				nodes = new ArrayList<Node>();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	@Override
	public String toString() {
		return "App [name=" + name + ", clientId=" + clientId + ", health="
				+ health + ", cpu=" + cpu + ", memory=" + memory + ", state="
				+ state + ", nodes=" + nodes + "]";
	}

}
