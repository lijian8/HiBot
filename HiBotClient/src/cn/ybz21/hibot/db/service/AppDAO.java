package cn.ybz21.hibot.db.service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;

import cn.ybz21.hibot.bean.App;
import cn.ybz21.hibot.bean.AppState;
import cn.ybz21.hibot.util.StaticValues;

public class AppDAO extends BaseDAO {

	private String NAME_TABLE = StaticValues.TABLE_APP;

	public App queryAppById(String id) {
		App tempApp = null;
		String sql = "select * from  " + NAME_TABLE + "  where id=?";
		Connection conn = db.getConnection();
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			pstmt = conn.prepareStatement(sql);
			int index = 1;
			pstmt.setObject(index++, id);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				tempApp = new App(rs);
			}
		} catch (SQLException e) {
			e.printStackTrace();

		}
		db.close(conn, pstmt, rs);
		return tempApp;
	}

	public List<App> queryRuningApp() {
		Connection con = db.getConnection();
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		List<App> apps = new ArrayList<App>();
		String sql = "select * from %s where %s = '%s' ";
		sql = String.format(sql, StaticValues.TABLE_APP, "state",
				AppState.RUNNING.toString());
		try {
			pstmt = con.prepareStatement(sql);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				App app = new App(rs);
				apps.add(app);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		db.close(con, pstmt, rs);
		return apps;
	}

	public void addApp(App app) {
		String sql = "insert into "
				+ StaticValues.TABLE_APP
				+ " (id,name,download_link,md5,cpu,memory,pid,start_cmd,stop_cmd,nodes,health,state,descr,version)"
				+ " values (?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		System.out.println(sql);
		Connection conn = db.getConnection();
		PreparedStatement pstmt = null;
		try {
			pstmt = conn.prepareStatement(sql);
			int index = 1;
			pstmt.setObject(index++, app.id);
			pstmt.setObject(index++, app.name);
			pstmt.setObject(index++, app.downloadLink);
			pstmt.setObject(index++, app.md5);
			pstmt.setObject(index++, app.cpu);
			pstmt.setObject(index++, app.memory);
			pstmt.setObject(index++, app.pid);
			pstmt.setObject(index++, app.startCmd);
			pstmt.setObject(index++, app.stopCmd);
			pstmt.setObject(index++, new Gson().toJson(app.nodes));
			pstmt.setObject(index++, app.health);
			pstmt.setObject(index++, app.state.toString());
			pstmt.setObject(index++, app.describe);
			pstmt.setObject(index++, app.version);
			pstmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		db.close(conn, pstmt, null);
	}

	public void updateAppState(App app) {
		String sql = "update " + StaticValues.TABLE_APP
				+ " set state=?,pid=? where id=?";
		Connection conn = db.getConnection();
		PreparedStatement pstmt = null;
		try {
			pstmt = conn.prepareStatement(sql);
			int index = 1;
			pstmt.setObject(index++, app.state.toString());
			pstmt.setObject(index++, app.pid);
			pstmt.setObject(index++, app.id);
			pstmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		db.close(conn, pstmt, null);
	}

	public static void main(String args[]) {
		AppDAO dao = new AppDAO();
		App app = dao.queryAppById("1");
		System.out.println(app);
		app.state = AppState.STOP;
		app.pid = "22";
		dao.updateAppState(app);
		app = dao.queryAppById("1");
		System.out.println(app);

		app.id = "5";
		dao.addApp(app);
		app = dao.queryAppById("5");
		System.out.println(app);
	}
}
